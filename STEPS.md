# Steps I've followed and where they've got me

1. Create a BitBucket Repo for your challenge
1. `YALO_ENV=staging yalo create project johnatha-challenge`
1. `cd johnatha-challenge `
1. `npm install`
1. `yalo create package johnatha-shop`
1. `cd packages/johnatha-shop`
1. `yalo preview`
1. `git remote add origin git@bitbucket.org:jfelix-yalo/johnatha-challenge.git` //needed as the
   remote address is not set when creating a project
1. `git add .`
1. `git commit -m "commit message"`
1. `git branch --set-upstream-to=origin/master master`
1. `git push —force` //needed because data won’t be pulled from BB when creating a project - do only
   if it’s okay to discard what you already have in BB, hopefully it will be an empty repo
1. `yalo addon:create -t theme` => changed the font sizes, primary and second colors and the font
   family
1. `yalo publish` - it does nothing, as there isn’t no function addon
1. `yalo addon:add -t function` //choose ‘externalSource’
    * Url: `https://api-staging2.yalochat.com/big-storage-ng/api/store/moissoup/products`
    * Headers: `{"Authorization":"Bearer
      eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJlenpuNlkyQ0dLVnlZaDdETTVKaXNvVjdheWxYd2wybCJ9.ypxUdJzvAUKZ7-hODuAcowk1o2n1R9MC4rTvYKhdqJ0”}`
    * Post: empty `()`
    * Pants:
      `name:name,sku:sku,description:description,category:category,ranking:ranking,imageURL:imageURL,price:price`
    * Order: default (`ranking:ASC`)
    * Transform: `tags:array`
    * productList: empty `()`
    * group products: category
1. tried `yalo publish` - it didn’t work, same output as previous attempt. maybe i can’t have just
   an external function, must i always implement one?
1. created a dummy function, `yalo addon:create -t function`, just to check if it publishes now -
   accepted default values or selected the first option
1. `yalo publish` didn’t work, weird error: `/Users/johnatha/.yalo/scripts/publish-functions.sh:
   line 83: $1: unbound variable` - checking that file, seems to be related to ui adding - maybe a
   bug because i didn’t publish my ui addon?
1. ah, actually, i don’t have any ui addons yet - i just changed the fonts, colors, etc. by with the
   addon i can change the way the elements are displayed - there’s still a bug in the previous step
   though
1. `yalo addon:create -t ui` to create a new addon - just follow the guide and create the
   `HorizontalProduct.tsx` file - this takes a long time to complete
1. remove dependecies from package json of dummyfunction - cat said it could cause some weird issues
   when publishing
1. try to `yalo publish` directly, maybe we don’t need to publish the ui addon
1. create an order config here with `ttl` of `3600` ->
   `https://studio.apollographql.com/sandbox/explorer?endpoint=https%3A%2F%2Fapi-staging2.yalochat.com%2Fstorefront-user%2Fv1%2Fuser%2Fstorefronts%3Ftoken%3DeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJKQURaQ1RRaE1GdUI2Rmc0eHRiT21Td0hRQmRnVkVUbSJ9.6ByX-cTvN1gnFDYuSoaYa3NVXV27_mWfIxrxtCmR3Lw&explorerURLState=N4IgJg9gxgrgtgUwHYBcQC4RxighigSwiQAIBZHfIpACgBIx9d0SB5AJzAXYGFiAzAgHMAKgE8ADggCSSCTgCEAShLAAOqRJR2CfAg5deA4TUZ4WDJivWaSJFCgA2AGg127BMG5IBfDT5BnEAA3XHYCXAAjRwQAZwwQGzs1cCYUliT3FIdHdJIAZgA2AAZi7z8kAJ8gA`
   - you need to complete your storefront’s url (next step)
1. `publish` was successful - it takes some time to go live at`
   https://sf-3365b1a7f68027cca4fcca14.yalochat.dev/johnatha-challenge/91cca020-547b-11ec-8fb6-4d4912414c31`
   - it will probably take about 15 minutes the first time the project is published - terraform
   creates a bunch of stuff in the background, so we must wait
1. the storefront is not found, though. the endpoint that queries storefronts
   (`/storefront-user/v1/user/storefronts`) returns an error `"Storefront by name johnatha-challenge
   Not Found”` - need to make sure that the storefront is created
1. Try a POST request at
   `https://api-staging2.yalochat.com/getCatalog/getCatalog?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJKQURaQ1RRaE1GdUI2Rmc0eHRiT21Td0hRQmRnVkVUbSJ9.6ByX-cTvN1gnFDYuSoaYa3NVXV27_mWfIxrxtCmR3Lw`
   with the following payload:
    ```json
    {
        "storefront": {
            "config": [
                {
                    "type": "function",
                    "name": "dummyFunction",
                    "status": "ENABLED",
                    "required": true,
                    "metadata": {
                        "trigger": {
                            "context": "user",
                            "event": "default",
                            "query": "storefront"
                        },
                        "entryPoint": "helloHttp",
                        "description": "",
                        "protocol": "https",
                        "method": "GET",
                        "path": "helloHttp",
                        "input": [
                            {
                                "dummy": "string"
                            }
                        ],
                        "output": "[xchema.Collection]",
                        "parameters": {
                            "url": "https://api-staging2.yalochat.com/storefront-user-dev/v1/user/storefronts-dev",
                            "token": "value"
                        }
                    }
                }
            ],
            "collections": [
                "5fbc26468ef6b771046f6b73",
                "5fbc26468ef6b7d0236f6b74",
                "5fbc26468ef6b7047f6f6b75",
                "5fbc26468ef6b78a936f6b76",
                "5fbc26468ef6b7132b6f6b77"
            ],
            "_id": "60b58ba51b521c41a42f991a",
            "name": "overwrite",
            "region": "us-east-1",
            "status": "CREATING",
            "currency": "MXN",
            "currencyPrefix": "$",
            "logo": "",
            "customer": "testfunc",
            "endpoints": [],
            "createdAt": "2021-06-01T01:21:41.189Z",
            "updatedAt": "2021-06-01T18:02:54.772Z",
            "__v": 0,
            "currencyFormat": "#,###0.00",
            "enabledCustomCart": false,
            "enabledOrderNote": true,
            "enabledProductDetail": true
            },
            "orderConfig": {
                "botSlug": "",
                "ttl": "7200",
                "userID": "",
                "id": "96f13340-c303-11eb-b7c7-7b8af8449d6b",
                "source": "internal",
                "metadata": {
                    "favorites": [
                        "5fbc26458ef6b7dad76f6b69",
                        "5fbc26468ef6b79f876f6b71"
                    ]
                },
            "collections": "5fbc26468ef6b771046f6b73,5fbc26468ef6b7d0236f6b74,5fbc26468ef6b7047f6f6b75,5fbc26468ef6b78a936f6b76,5fbc26468ef6b7132b6f6b77",
            "channelID": "",
            "status": "CREATED",
            "state": "default",
            "channelName": "whatsapp"
        },
        "variables": {
            "name": "overwrite",
            "configId": "96f13340-c303-11eb-b7c7-7b8af8449d6b"
        }
    }
    ```
1. It returns an error. Maybe something in the URL?
    ```json
    {
        "error": "Error missing getCatalog in storefront config {\"config\":[{\"type\":\"function\",\"name\":\"dummyFunction\",\"status\":\"ENABLED\",\"required\":true,\"metadata\":{\"trigger\":{\"context\":\"user\",\"event\":\"default\",\"query\":\"storefront\"},\"entryPoint\":\"helloHttp\",\"description\":\"\",\"protocol\":\"https\",\"method\":\"GET\",\"path\":\"helloHttp\",\"input\":[{\"dummy\":\"string\"}],\"output\":\"[xchema.Collection]\",\"parameters\":{\"url\":\"https://api-staging2.yalochat.com/storefront-user-dev/v1/user/storefronts-dev\",\"token\":\"value\"}}}],\"collections\":[\"5fbc26468ef6b771046f6b73\",\"5fbc26468ef6b7d0236f6b74\",\"5fbc26468ef6b7047f6f6b75\",\"5fbc26468ef6b78a936f6b76\",\"5fbc26468ef6b7132b6f6b77\"],\"_id\":\"60b58ba51b521c41a42f991a\",\"name\":\"overwrite\",\"region\":\"us-east-1\",\"status\":\"CREATING\",\"currency\":\"MXN\",\"currencyPrefix\":\"$\",\"logo\":\"\",\"customer\":\"testfunc\",\"endpoints\":[],\"createdAt\":\"2021-06-01T01:21:41.189Z\",\"updatedAt\":\"2021-06-01T18:02:54.772Z\",\"__v\":0,\"currencyFormat\":\"#,###0.00\",\"enabledCustomCart\":false,\"enabledOrderNote\":true,\"enabledProductDetail\":true}"
    }
    ```
1. Tried changing the URL to
   `https://api-staging2.yalochat.com/dummyFunction/dummyFunction?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJKQURaQ1RRaE1GdUI2Rmc0eHRiT21Td0hRQmRnVkVUbSJ9.6ByX-cTvN1gnFDYuSoaYa3NVXV27_mWfIxrxtCmR3Lw`
   but started getting different errors (`Error missing getProducts in storefront config`) so
   probably the URL is not the issue.
1. Let's try create the storefront directly from the API.
1. In the middle of creating a mutation to create a storefront, I decide to query the storefront ID
   I see in my `package-config.yml` and it actually exists, but with name `johnatha-shop`, which is
   actually the name I've set in the package config. So why is it trying to load
   `johnatha-challenge` instead? Is it because of the repo name?
1. It turns out that it was a stupid mistake on my end... In the URL, I set the wrong storefront
   name... I was querying
   `https://sf-3365b1a7f68027cca4fcca14.yalochat.dev/johnatha-challenge/c8792e60-569a-11ec-8fb6-4d4912414c31`
   instead of
   `https://sf-3365b1a7f68027cca4fcca14.yalochat.dev/johnatha-shop/c8792e60-569a-11ec-8fb6-4d4912414c31`
1. Accessing the correct URL now and I face the issue where my catalog is not being loaded... The
   endpoint that gets the catalog actually returns an empty list. It looks like it queries the
   server for a catalog with a name of `johnatha-shop`. I will check what's in that _table_ (need to
   learn GraphQL terminology).
1. The catalog for `johnatha-shop` doesn't exist indeed - I thought it could be an issue with the
   data inserted there, maybe wrong schema... not sure how the catalog gets created 😞. I will try
   to create one directly from Apollo - except that there's no endpoint to create a mutation for
   catalog 🤯
1. A look into the `collections` _table_ and it seems that it has the same ID as collections...
   could they be somehow the same thing? I will create a collection then.
1. Created a collection with the following payload (id `61ae28738fc6af2bbe48a26a`):
    ```json
    {
    "data": {
        "name": "johnatha-shop",
        "products": [
            "5f0390f2e833b147c071874d",
            "5f3ac61817a40508ab15ce4d"
            ],
        "status": "ENABLED",
        "schedule": "dailyAfternoon",
        "ranking": 1,
        "enabledPrice": true,
        "imageURL": ["https://assets.website-files.com/5cabb6794998de00739f1740/5f931cbd29abf11f2fce7232_ico_whatsapp.svg"]
    }
    }
    ```
1. The catalog still doesn't exist, though 😕
1. I noticed my storefront is missing some properties, such as `currency` and `curencyPrefix`.
   Perhaps I should add them directly in Apollo? I'll try.
1. I run a mutation operation with the following payload:
    ```json
    {
    "data": {
        "currencyPrefix": "R$",
        "currency": "BRL"
    }
    }
    ```
1. However, my storefront is not updated. It's still missing the `currency` and `currencyPrefix`
   properties. Maybe a new `publish` might fix this issue?
1. It didn't work. I just realized that my package config is missing those properties. I will try
   adding the properties there and running a new publish.
1. That didn't work either! 😲
1. Tried updating the storefront from Apollo again and now it worked. I think I've missed the
   storefront ID the first time I've tried. This payload worked:
    ```json
    {
        "updateStorefrontId": "61aa70188fc6af7cdd48a267",
        "data": {
            "currency": "BRL",
            "currencyFormat": "#,##0.00",
            "currencyPrefix": "R$"
        }
    }
    ```
1. Tried to add `collections` to the package config. `collections: ["61ae28738fc6af2bbe48a26a"]` and
   publish again.
1. THE PRODUCTS ARE FINALLY SHOWING! 🥳🥳🥳🥳🥳🥳🥳🥳🥳🥳🥳🥳🥳
1. However, there's probably a way to make this all work automatically, which I haven't figured out
   yet... For example, right now, I have to set prices to my products and I think it would make
   sense to get those from an external source of some sort and the publish script would take care of
   it.
1. I created the following prices: (ids are: `61ae40238fc6afb28c48a26b` and
   `61ae40348fc6af80b248a26c`)
    ```json
    [
        {
        "collectionID": "61ae28738fc6af2bbe48a26a",
        "productID": "5f0390f2e833b147c071874d",
        "price": 18.99
        },
        {
        "collectionID": "61ae28738fc6af2bbe48a26a",
        "productID": "5f3ac61817a40508ab15ce4d",
        "price": 20.75
        }
    ]
    ```
1. I can see the prices in the storefront now. However, I don't like that we show the `currency` and
   the `currencyPrefix` in the price. Ideally, we should show only the prefix. (e.g `R$ BRL 18.99`
   -> `R$ 18.99`). I will try to change it in the UI addon.
1. The changes on how the prices look like worked. But now I should change the format, as we use
   commas as a decimal separator in Brazil. So the format should be `'#.##0,00'` instead of
   `'#,##0.00'` - I expect to see the price as `R$ 18,99` after this change.
1. That didn't work - perhaps the format is not being applied or it's a region thing. It works as I
   expected in [this jsfiddle](https://jsfiddle.net/Mottie/t2etyodx/) though. It's minor, so I won't
   stress much about that.
1. Okay, let's try a function addon again. Will create a new one, returning a fixed set of products,
   to reduce complexity.
    * It doesn't make much sense, though. A collection contains a list of product IDs... how am I
      supposed to populated that field?
    * Perhaps I don't need to set the IDs there? Maybe the schema is a bit different?
1. I _fork_ the `externalSource` function
   (`https://bitbucket.org/yalochat/engyalo-yalo-cli/src/0dae15b99ca2e87f152c2b1f2c5e08ca7f301200/shared/packages/commons/functions/externalsource/index.js?at=master`)
   to know how it works and replace the call to `retrieveProducts` with the products I've got from
   `https://api-staging2.yalochat.com/big-storage-ng/api/store/moissoup/products`. A necessary
   change is to add this `id:_id` to the `setters` list.
1. Then I run my modified version of `externalSource` and put its output in my new function,
   `getCatalog2`. I will publish it now and see if it works. I will also remove `dummyFunction`.
1. It publishes but it looks like my function is not being used at all. It still loads the old
   products.
1. Byron suggested using the externalSource function again and I noticed I had the `setters` wrong -
   I was missing the `id:_id` property. Tried publishing it and now I can't load the storefront.
1. Tweaking the package-config again and trying to publish again.
1. It's loading again but I couldn't make the `externalSource` function work - it always loads the
   products from the `collection` instead. I think I've set everything correctly but for some reason
   is not being called or returning an error, causing the catalog to fallback to the collections
   list. Since it's been 6 days since I started the challenge, I'll stop it here.

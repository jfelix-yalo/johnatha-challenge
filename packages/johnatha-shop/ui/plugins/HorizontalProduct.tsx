import React from 'react';
import { ICollectionProduct } from '@engyalo/storefront-types';
import { ImageBox } from '@engyalo/storefront-components';
import { Grid, Box, Heading, Text } from '@chakra-ui/core';

export const HorizontalProduct = ({
    product,
    currency,
    currencyPrefix,
    cart,
    ...props
}: ICollectionProduct) => {
    return (
        <Grid gridTemplateColumns="1fr 3fr" borderY="3px solid #F1F1F1" py="10px">
            <Box>
                <ImageBox imageUrl={product?.imageURL?.[0] ?? ''} />
            </Box>
            <Box>
                <Heading as="h3" size="sm">
                    {product.name}
                </Heading>
                <Text fontFamily="body" color="primary.50" my="2">
                    {product.description}
                </Text>
                <Text fontFamily="body" fontSize="md" color="secondary.50">
                    {currencyPrefix} {product.price}
                </Text>
            </Box>
        </Grid>
    );
}

import { StorefrontPlugins } from '@engyalo/storefront-types'
import { Grid } from '@chakra-ui/core'
import { HorizontalProduct } from './plugins/HorizontalProduct'

export const plugins = {
  Catalogue: {
      container: Grid,
      props: {
          gridTemplateColumns: '1fr',
      },
  },
  CollectionProduct: {
      container: HorizontalProduct,
  },
  
} as StorefrontPlugins

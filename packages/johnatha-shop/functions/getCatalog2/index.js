const ADDON_NAME = 'getCatalog2';
const ADDON_TYPE = 'function';

/**
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 */
exports.getCatalog2 = (req, res) => {
  const {
    body: { storefront, orderConfig },
  } = req;
  if (!storefront) {
    res
      .status(400)
      .send({
        error: `Error missing storefront in body ${JSON.stringify(req.body)}`,
      });
    return;
  }

  if (!orderConfig) {
    res
      .status(400)
      .send({
        error: `Error missing orderConfig in body ${JSON.stringify(req.body)}`,
      });
    return;
  }

  const { config } = storefront;
  const addonConfig = config.find(
    (configuration) =>
      configuration.name === ADDON_NAME && configuration.type === ADDON_TYPE
  );

  if (!addonConfig) {
    res
      .status(400)
      .send({
        error: `Error missing ${ADDON_NAME} in storefront config ${JSON.stringify(
          storefront
        )}`,
      });
    return;
  }

  res.status(200).send([
    {
      id: 1,
      name: 'SABRITAS PC',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af6a',
          name: 'SABRITAS SAL 42GR',
          sku: '010101',
          description: 'SABRITAS SAL 42GR',
          category: 'SABRITAS PC',

          ranking: 14,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010101.png',
          ],
          clientType: null,
          price: 11.99,
        },
        {
          id: '614a49f79f659c1d5784af6c',
          name: 'SABRITAS ADOBADAS 42GR',
          sku: '010133',
          description: 'SABRITAS ADOBADAS 42GR',
          category: 'SABRITAS PC',
          ranking: 15,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010133.png',
          ],
          clientType: null,
          price: 11.99,
        },
        {
          id: '614a49f79f659c1d5784af6b',
          name: 'SABRITAS C&E 42GR',
          sku: '010132',
          description: 'SABRITAS C&E 42GR',
          category: 'SABRITAS PC',
          ranking: 16,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010132.png',
          ],
          clientType: null,
          price: 11.99,
        },
        {
          id: '614a49f79f659c1d5784af6d',
          name: 'SABRITAS FLAMIN 42GR',
          sku: '010143',
          description: 'SABRITAS FLAMIN 42GR',
          category: 'SABRITAS PC',
          ranking: 17,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010143.png',
          ],
          clientType: null,
          price: 11.99,
        },
      ],
      totalProducts: 4,
    },
    {
      id: 2,
      name: 'RUFFLES',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af6e',
          name: 'RUFFLES QUESO 50GR',
          sku: '010310',
          description: 'RUFFLES QUESO 50GR',
          category: 'RUFFLES',
          ranking: 3,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010310.png',
          ],
          clientType: null,
          price: 11.99,
        },
      ],
      totalProducts: 1,
    },
    {
      id: 3,
      name: 'DORITOS',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af6f',
          name: 'DORITOS NACHO 58GR',
          sku: '010508',
          description: 'DORITOS NACHO 58GR',
          category: 'DORITOS',
          ranking: 4,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010508.png',
          ],
          clientType: null,
          price: 11.13,
        },
        {
          id: '614a49f79f659c1d5784af72',
          name: 'DORITO DINAM CHIL Y LIM 72GR',
          sku: '010556',
          description: 'DORITO DINAM CHIL Y LIM 72GR',
          category: 'DORITOS',
          ranking: 5,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010556.png',
          ],
          clientType: null,
          price: 9.42,
        },
        {
          id: '614a49f79f659c1d5784af71',
          name: 'DORITOS FLAMIN 58GR',
          sku: '010511',
          description: 'DORITOS FLAMIN 58GR',
          category: 'DORITOS',
          ranking: 6,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010511.png',
          ],
          clientType: null,
          price: 11.13,
        },
        {
          id: '614a49f79f659c1d5784af70',
          name: 'DORITOS INCOGNITA 58GR',
          sku: '010510',
          description: 'DORITOS INCOGNITA 58GR',
          category: 'DORITOS',
          ranking: 7,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010510.png',
          ],
          clientType: null,
          price: 11.13,
        },
      ],
      totalProducts: 4,
    },
    {
      id: 4,
      name: 'FRITOS',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af73',
          name: 'FRITOS SAL 57GR',
          sku: '010604',
          description: 'FRITOS SAL 57GR',
          category: 'FRITOS',
          ranking: 22,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/010604.png',
          ],
          clientType: null,
          price: 10.27,
        },
      ],
      totalProducts: 1,
    },
    {
      id: 9,
      name: 'PAKETAXO',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af7d',
          name: 'PAKETAXO MEZCLADITO 70GR',
          sku: '443295',
          description: 'PAKETAXO MEZCLADITO 70GR',
          category: 'PAKETAXO',
          ranking: 18,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/443295.png',
          ],
          clientType: null,
          price: 11.13,
        },
        {
          id: '614a49f79f659c1d5784af74',
          name: 'PAKETAXO XTRA FLAMIN 65GR',
          sku: '013294',
          description: 'PAKETAXO XTRA FLAMIN 65GR',
          category: 'PAKETAXO',
          ranking: 19,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/013294.png',
          ],
          clientType: null,
          price: 11.13,
        },
        {
          id: '614a49f79f659c1d5784af75',
          name: 'PAKETAXO BOTANERO 70GR',
          sku: '033291',
          description: 'PAKETAXO BOTANERO 70GR',
          category: 'PAKETAXO',
          ranking: 20,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/033291.png',
          ],
          clientType: null,
          price: 11.13,
        },
        {
          id: '614a49f79f659c1d5784af76',
          name: 'PAKETAXO QUEXO 65GR',
          sku: '033293',
          description: 'PAKETAXO QUEXO 65GR',
          category: 'PAKETAXO',
          ranking: 21,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/033293.png',
          ],
          clientType: null,
          price: 11.13,
        },
      ],
      totalProducts: 4,
    },
    {
      id: 6,
      name: 'RANCHERITOS',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af77',
          name: 'RANCHERITOS 56GR',
          sku: '041109',
          description: 'RANCHERITOS 56GR',
          category: 'RANCHERITOS',
          ranking: 23,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/041109.png',
          ],
          clientType: null,
          price: 10.27,
        },
      ],
      totalProducts: 1,
    },
    {
      id: 7,
      name: 'TOSTITOS',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af78',
          name: 'TOSTITOS SALSA V 65GR',
          sku: '051724',
          description: 'TOSTITOS SALSA V 65GR',
          category: 'TOSTITOS',
          ranking: 1,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/051724.png',
          ],
          clientType: null,
          price: 10.27,
        },
        {
          id: '614a49f79f659c1d5784af79',
          name: 'TOSTITOS FLAMIN 65GR',
          sku: '051743',
          description: 'TOSTITOS FLAMIN 65GR',
          category: 'TOSTITOS',
          ranking: 2,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/051743.png',
          ],
          clientType: null,
          price: 10.27,
        },
      ],
      totalProducts: 2,
    },
    {
      id: 8,
      name: 'CHEETOS BAKED',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af7a',
          name: 'CHEETOSPOFFS 38',
          sku: '100582',
          description: 'CHEETOSPOFFS 38',
          category: 'CHEETOS BAKED',
          ranking: 12,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/100582.png',
          ],
          clientType: null,
          price: 8.56,
        },
        {
          id: '614a49f79f659c1d5784af7b',
          name: 'CHEETOSBOLI 42',
          sku: '100583',
          description: 'CHEETOSBOLI 42',
          category: 'CHEETOS BAKED',
          ranking: 13,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/100583.png',
          ],
          clientType: null,
          price: 8.56,
        },
      ],
      totalProducts: 2,
    },
    {
      id: 9,
      name: 'CHEETOS FRIED',
      status: 'ENABLED',
      enabledPrice: true,
      productsPriced: [
        {
          id: '614a49f79f659c1d5784af7e',
          name: 'CHEETOS FLAMIN 52GR',
          sku: '470943',
          description: 'CHEETOS FLAMIN 52GR',
          category: 'CHEETOS FRIED',
          ranking: 8,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/470943.png',
          ],
          clientType: null,
          price: 8.56,
        },
        {
          id: '614a49f79f659c1d5784af80',
          name: 'CHEETOS NACHO 52GR',
          sku: '470949',
          description: 'CHEETOS NACHO 52GR',
          category: 'CHEETOS FRIED',
          ranking: 9,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/470949.png',
          ],
          clientType: null,
          price: 8.56,
        },
        {
          id: '614a49f79f659c1d5784af7f',
          name: 'CHEETOS TORCIDITOS 52GR',
          sku: '470946',
          description: 'CHEETOS TORCIDITOS 52GR',
          category: 'CHEETOS FRIED',
          ranking: 10,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/470946.png',
          ],
          clientType: null,
          price: 8.56,
        },
        {
          id: '614a49f79f659c1d5784af7c',
          name: 'CHEETOS SHOTS FLAMIN 27GR',
          sku: '380995',
          description: 'CHEETOS SHOTS FLAMIN 27GR',
          category: 'CHEETOS FRIED',
          ranking: 11,
          imageURL: [
            'https://storage.googleapis.com/yalobots/PepsiCoMX/380995.png',
          ],
          clientType: null,
          price: 4.3,
        },
      ],
      totalProducts: 4,
    },
  ]);
};

# Function getCatalog2
===================



The function is write in Node.js and it's a HTTP Function. For details [see Writing HTTP Functions.](https://cloud.google.com/functions/docs/writing/http)

## Structuring source code

```
.
├── node_modules
└── index.js
└── package.json

```

- node_modules (Default installed packages to makes the function works)
- index.js (It's the function's code to allows modify the behavior of a storefront flow)
- package.json (Here are going to be installed all dependecies to be used in the function)


### dependencies
For details [see Writing HTTP Functions.](https://cloud.google.com/functions/docs/writing/specifying-dependencies-nodejs)

# Feedback

## CLI setup

It would be nice if the CLI took care of installing the missing tools and give warnings about the
ones already installed but with a different version than required. I understand this is not trivial,
especially depending on how many OSes we want to support but I feel like there's room for
improvement there.

## Difficult to find documentation

It was hard for me to do the challenge by myself, in the sense that many things were either not
explained in the docs or incomplete.

Interestingly, there was a discrepancy there: for UI addons, for instance, there's a good example on
how to customize one and how to use Chakra components but function addons are barely mentioned.

Both of them (UI and function addons) are very important from my point of view, as I want to
customize how my storefront looks and make it more resemble my brand, but I also want to have a
dynamic product listing, that could depend on the user location, for example.

I took a look at repositories that had some examples of storefront implementations, so I could have
a better idea of which direction to follow.


## Not-so-great developer experience with the CLI

For me, it looks like the CLI was not intended for someone that is not technical and that's okay.
Though I think such CLI would be much more powerful if it did some things automatically and
abstracted a lot of things for the end-user.

The CLI operations are just very slow: `yalo preview` is slow, `yalo publish` is slow, `yalo
addon:create -t ui` is slow... If I had to run those commands just a couple of times that would
probably be okay but since I had to try it out multiple times, it consumed a big part of my day.

I would expect some hot/live reload, at least on preview mode.

The output of `yalo create project my-project` ends with something like this:

```sh
initializing git... done

please execute the following commands:
cd my-project
```
It should probably include further instructions, such as `npm install`, which is required, but not
listed. And, naturally, it could just `cd` that for you.

Plenty of warnings when running some commands, such as `yallo addon:create -t ui`. Probably because
this is v1 and we're not putting much effort into it anymore but something we should keep an eye on
in the newer version. If anything goes wrong, I wouldn't be sure if it's something I did or because
of any warnings like these:
```sh
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated request-promise-native@1.0.9: request-promise-native has been deprecated because it extends the now deprecated request package, see https://github.com/request/request/issues/3142

npm WARN deprecated har-validator@5.1.5: this library is no longer supported

npm WARN deprecated fsevents@1.2.13: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
```

At some point I've faced some weirdness with required Node.js versions: `yalo preview` required
`v12.18.3` and an inner command spawned by preview required `v12.18.2`. I honestly don't remember
how I fixed that - maybe with `yalo update` - but it didn't happen anymore. Anyway, just something I
noticed.


`yalo preview` also always asked for a config, perhaps we could store that somewhere so we wouldn't
have to inform it again:
```sh
 Launching 'config' addons config
? ttl: 7200
? source internal
? channelID:
? channelName: whatsapp
? botSlug:
? state: default
? userID:
? metadata:
? collections: Received
```

## What I would like to see

### A good documentation on add-ons
From my point of view, add-ons are key to a successful implementation of a storefront: that is how I
can make it my own.

UI add-ons allow me to customize my catalog presentation, so I can provide the best experience
possible to my customers.

Functions add-ons have a lot of potential: imagine how many things we could do with that? From
dynamic catalogs, to promotions (like take 2 and pay 1, or some percentage off), automatically
controlling stock and all of that.

For this reason, we should have comprehensive documentation on how those work, explaining all those
properties that the CLI asks us. 

### Templates vs CLI configuration
When you create an add-on, the CLI asks for a bunch of properties that I didn't have an idea of what
they represent. What if we just run `yalo addon:create -t ui {add-on name}` and then it created a
file with the default values and an explanation of what every field meant and how to do simple
things? That would make it much easier to try it out since it would save us a bunch of discovery
time. 

Plus, seeing something work already and then tweaking that file makes it much easier for people to
walk on their own feet. 

So what if instead of just loading the default UI and the default catalog we had add-ons created
automatically for both of those? That would make it super clear on how to use a function add-on and
a UI add-on.

### UI layouts
Though UI add-ons are powerful and we can do anything with them, the storefront will be displayed in
a mobile device and there are only so many combinations of how a catalog can look good in smaller
screens.

So perhaps we could provide layouts out of the box, like a list layout and then a grid layout.
Customers would be able to customize those but the skeleton would be the same. Yes, that would limit
the possibilities on how a storefront can look like but some layouts are proven to look good on a
mobile screen and a lot of stores use them anyway.

### Handy function add-ons 
Similarly to UI layouts, there are operations that many stores are likely to use, such as the
`externalSource` function. 

Examples on how to use them would be great and, again, if instead of just hiding the implementations
of the add-ons and loading default ones we just _showed_ the actual code, that would greatly improve
the customization productivity. 
